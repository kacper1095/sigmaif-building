import tensorflow as tf
from tensorflow.python.client import device_lib

print("Version: ", tf.__version__)

a = tf.load_op_library('/projects/sigmaif/build/libsigma_if.so')
print(dir(a))

print("\nIs GPU available: ")
print(tf.test.is_gpu_available())

print("\nAvailable devices: ")
print(device_lib.list_local_devices())
