#if GOOGLE_CUDA

#define EIGEN_USE_GPU

#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/framework/register_types.h"
#include "tensorflow/core/framework/shape_inference.h"
#include "tensorflow/core/framework/tensor.h"
#include "tensorflow/core/framework/tensor_shape.h"
#include "tensorflow/core/framework/types.h"
#include "tensorflow/core/platform/default/logging.h"
#include "tensorflow/core/platform/types.h"
#include "tensorflow/core/util/cuda_launch_config.h"
#include "tensorflow/core/util/work_sharder.h"
#include "third_party/eigen3/unsupported/Eigen/CXX11/Tensor"
#include <cuda.h>
#include <iostream>

namespace tensorflow {

void SigmaIfKernelLauncher(const float *input, const float *weights,
                           const float *threshold,
                           const float *forward_fading_factor,
                           const int *groups, const int *group_classes,
                           float *result, int *used_neurons,
                           const int batch_samples, const int units,
                           const int features, const cudaStream_t &stream);

void SortIfNecessaryKernelLauncher(const bool *resort, const float *weights,
                                   const int *groups, int *sorted_groups,
                                   const int features, const int units,
                                   const cudaStream_t &stream);

void GradKernelLauncher(const float *grads, const float *inputs,
                        const float *weights,
                        const float *backward_fading_factor,
                        const int *used_neurons, const int *groups,
                        const int *group_classes, float *grad_weights,
                        const float *grad_inputs, const int batch_samples,
                        const int units, const int features,
                        const cudaStream_t &stream);

class SigmaIfOpGPU : public OpKernel {
public:
  explicit SigmaIfOpGPU(OpKernelConstruction *context) : OpKernel(context) {}

  void Compute(OpKernelContext *context) override {
    DCHECK_EQ(8, context->num_inputs());
    std::cout << "Lollllll" << std::endl;

    const Tensor &input = context->input(0);
    const Tensor &weights = context->input(1);
    const Tensor &threshold = context->input(2);
    const Tensor &forward_fading_factor = context->input(3);
    const Tensor &groups = context->input(5);
    const Tensor &group_classes = context->input(6);
    const Tensor &resort_groups = context->input(7);

    const TensorShape &input_shape = input.shape();
    const TensorShape &weights_shape = weights.shape();

    const cudaStream_t &stream = GetCudaStream(context);

    DCHECK_EQ(input_shape.dims(), 2);
    DCHECK_EQ(weights_shape.dims(), 2);
    DCHECK_EQ(input_shape.dim_size(1), weights_shape.dim_size(0));

    DCHECK_EQ(threshold.dims(), 0);
    DCHECK_EQ(forward_fading_factor.dims(), 0);
    DCHECK_EQ(groups.dims(), 2);
    DCHECK_EQ(groups.dim_size(0), weights.dim_size(1));
    DCHECK_EQ(groups.dim_size(1), weights.dim_size(0));

    const int batch_samples = input_shape.dim_size(0);
    const int features = input_shape.dim_size(1);
    const int units = weights_shape.dim_size(1);

    TensorShape output_shape;
    TensorShape used_neurons_shape;
    TensorShape sorted_groups_shape;

    output_shape.AddDim(batch_samples);
    output_shape.AddDim(units);

    used_neurons_shape.AddDim(batch_samples);
    used_neurons_shape.AddDim(units);

    sorted_groups_shape.AddDim(units);
    sorted_groups_shape.AddDim(features);

    Tensor *output = nullptr;
    Tensor *used_neurons = nullptr;
    Tensor *sorted_indices = nullptr;

    OP_REQUIRES_OK(context, context->allocate_output(0, output_shape, &output));
    OP_REQUIRES_OK(context, context->allocate_output(1, used_neurons_shape,
                                                     &used_neurons));
    OP_REQUIRES_OK(context, context->allocate_output(2, sorted_groups_shape,
                                                     &sorted_indices));

    auto input_flat_ptr = input.flat<float>().data();
    auto weights_flat_ptr = weights.flat<float>().data();
    auto groups_flat_ptr = groups.flat<int>().data();
    auto threshold_scalar_ptr = threshold.flat<float>().data();
    auto forward_fading_factor_scalar_ptr =
        forward_fading_factor.flat<float>().data();
    auto group_classes_vector_ptr = group_classes.flat<int>().data();
    auto resort_groups_scalar_ptr = resort_groups.flat<bool>().data();

    auto output_flat_ptr = output->flat<float>().data();
    auto used_neurons_flat_ptr = used_neurons->flat<int>().data();
    auto sorted_indices_tensor_ptr = sorted_indices->flat<int>().data();

    SortIfNecessaryKernelLauncher(resort_groups_scalar_ptr, weights_flat_ptr,
                                  groups_flat_ptr, sorted_indices_tensor_ptr,
                                  features, units, stream);

    SigmaIfKernelLauncher(
        input_flat_ptr, weights_flat_ptr, threshold_scalar_ptr,
        forward_fading_factor_scalar_ptr, sorted_indices_tensor_ptr,
        group_classes_vector_ptr, output_flat_ptr, used_neurons_flat_ptr,
        batch_samples, units, features, stream);
  }
};

class SigmaIfGradOpGPU : public OpKernel {
public:
  explicit SigmaIfGradOpGPU(OpKernelConstruction *context)
      : OpKernel(context) {}

  void Compute(OpKernelContext *context) override {
    DCHECK_EQ(context->num_inputs(), 7);
    const Tensor &grad = context->input(0);
    const Tensor &input = context->input(1);
    const Tensor &weights = context->input(2);
    const Tensor &backward_fading_factor = context->input(3);
    const Tensor &group_classes = context->input(4);
    const Tensor &used_neurons = context->input(5);
    const Tensor &groups = context->input(6);

    const cudaStream_t &stream = GetCudaStream(context);

    TensorShape grad_shape = grad.shape();
    TensorShape input_shape = input.shape();
    TensorShape weights_shape = weights.shape();

    Tensor *grad_input = nullptr;
    Tensor *grad_weights = nullptr;

    OP_REQUIRES_OK(context,
                   context->allocate_output(0, input_shape, &grad_input));
    OP_REQUIRES_OK(context,
                   context->allocate_output(1, weights_shape, &grad_weights));

    auto grad_tensor_ptr = grad.flat<float>().data();
    auto input_tensor_ptr = input.flat<float>().data();
    auto weights_tensor_ptr = weights.flat<float>().data();
    auto used_neurons_tensor_ptr = used_neurons.flat<int>().data();
    auto groups_tensor_ptr = groups.flat<int>().data();
    auto backward_fading_factor_scalar_ptr =
        backward_fading_factor.flat<float>().data();
    auto group_classes_vector_ptr = group_classes.flat<int>().data();

    auto grad_input_tensor_ptr = grad_input->flat<float>().data();
    auto grad_weights_tensor_ptr = grad_weights->flat<float>().data();

    int nb_features = input_shape.dim_size(1);
    int batch_samples = input_shape.dim_size(0);
    int units = weights_shape.dim_size(1);

    GradKernelLauncher(
        grad_tensor_ptr, input_tensor_ptr, weights_tensor_ptr,
        backward_fading_factor_scalar_ptr, used_neurons_tensor_ptr,
        groups_tensor_ptr, group_classes_vector_ptr, grad_weights_tensor_ptr,
        grad_input_tensor_ptr, batch_samples, units, nb_features, stream);
  }
};

REGISTER_KERNEL_BUILDER(Name("SigmaIf").Device(DEVICE_GPU), SigmaIfOpGPU);
REGISTER_KERNEL_BUILDER(Name("SigmaIfGrad").Device(DEVICE_GPU),
                        SigmaIfGradOpGPU);
} // namespace tensorflow

#endif // GOOGLE_CUDA