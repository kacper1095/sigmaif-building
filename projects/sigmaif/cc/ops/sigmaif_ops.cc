#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/shape_inference.h"


namespace tensorflow {

REGISTER_OP("SigmaIf")
    .Attr("T: realnumbertype = DT_FLOAT")
    .Input("input: T")
    .Input("weights: T")
    .Input("threshold: T")
    .Input("forward_fading_factor: T")
    .Input("backward_fading_factor: T")
    .Input("groups: int32")
    .Input("group_classes: int32")
    .Input("resort_groups: bool")
    .Output("result: T")
    .Output("used_neurons: int32")
    .Output("sorted_group_indices: int32")
    .SetShapeFn([](::tensorflow::shape_inference::InferenceContext *c) {
      shape_inference::ShapeHandle input_shape;
      TF_RETURN_IF_ERROR(c->WithRank(c->input(0), 2, &input_shape));

      shape_inference::ShapeHandle weight_shape;
      TF_RETURN_IF_ERROR(c->WithRank(c->input(1), 2, &weight_shape));

      shape_inference::ShapeHandle groups_shape;
      TF_RETURN_IF_ERROR(c->WithRank(c->input(5), 2, &groups_shape));

      shape_inference::ShapeHandle group_classes_shape;
      TF_RETURN_IF_ERROR(c->WithRank(c->input(6), 1, &group_classes_shape));

      shape_inference::DimensionHandle samples = c->Dim(input_shape, 0);
      shape_inference::DimensionHandle features = c->Dim(input_shape, 1);
      shape_inference::DimensionHandle units = c->Dim(weight_shape, 1);

      c->set_output(0, c->Matrix(samples, units));
      c->set_output(1, c->Matrix(samples, units));
      c->set_output(2, c->Matrix(units, features));

      return Status::OK();
    });

REGISTER_OP("SigmaIfGrad")
    .Attr("T: realnumbertype = DT_FLOAT")
    .Input("grad: T")
    .Input("input: T")
    .Input("weights: T")
    .Input("backward_fading_factor: T")
    .Input("group_classes: int32")
    .Input("used_neurons: int32")
    .Input("sorted_groups: int32")
    .Output("grad_input: T")
    .Output("grad_weights: T");

} // namespace tensorflow