#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/op_kernel.h"
#include "tensorflow/core/platform/default/logging.h"

#include "sigmaif_op.h"

namespace tensorflow {

template <typename Device, typename Dtype> class SigmaIfOp : public OpKernel {
public:
  explicit SigmaIfOp(OpKernelConstruction *context) : OpKernel(context) {}

  void Compute(OpKernelContext *context) override {
    DCHECK_EQ(8, context->num_inputs());

    const Tensor &input = context->input(0);
    const Tensor &weights = context->input(1);
    const Tensor &threshold = context->input(2);
    const Tensor &forward_fading_factor = context->input(3);
    const Tensor &groups = context->input(5);
    const Tensor &group_classes = context->input(6);
    const Tensor &resort_groups = context->input(7);

    const TensorShape &input_shape = input.shape();
    const TensorShape &weights_shape = weights.shape();

    DCHECK_EQ(input_shape.dims(), 2);
    DCHECK_EQ(weights_shape.dims(), 2);
    DCHECK_EQ(input_shape.dim_size(1), weights_shape.dim_size(0));

    DCHECK_EQ(threshold.dims(), 0);
    DCHECK_EQ(forward_fading_factor.dims(), 0);
    DCHECK_EQ(groups.dims(), 2);
    DCHECK_EQ(groups.dim_size(0), weights.dim_size(1));
    DCHECK_EQ(groups.dim_size(1), weights.dim_size(0));

    const int batch_samples = input_shape.dim_size(0);
    const int features = input_shape.dim_size(1);
    const int units = weights_shape.dim_size(1);

    TensorShape output_shape;
    TensorShape used_neurons_shape;
    TensorShape sorted_groups_shape;

    output_shape.AddDim(batch_samples);
    output_shape.AddDim(units);

    used_neurons_shape.AddDim(batch_samples);
    used_neurons_shape.AddDim(units);

    sorted_groups_shape.AddDim(units);
    sorted_groups_shape.AddDim(features);

    Tensor *output = nullptr;
    Tensor *used_neurons = nullptr;
    Tensor *sorted_indices = nullptr;

    OP_REQUIRES_OK(context, context->allocate_output(0, output_shape, &output));
    OP_REQUIRES_OK(context, context->allocate_output(1, used_neurons_shape,
                                                     &used_neurons));
    OP_REQUIRES_OK(context, context->allocate_output(2, sorted_groups_shape,
                                                     &sorted_indices));

    ::tensorflow::functor::SortIndicesFunctor<Device, Dtype>::launch(
        context, weights, groups, resort_groups, sorted_indices, units,
        features);

    ::tensorflow::functor::SigmaIfFunctor<Device, Dtype>::launch(
        context, input, weights, sorted_indices, threshold,
        forward_fading_factor, group_classes, output, used_neurons,
        batch_samples, units, features);
  }
};

template <typename Device, typename Dtype>
class SigmaIfGradOp : public OpKernel {
public:
  explicit SigmaIfGradOp(OpKernelConstruction *context) : OpKernel(context) {}

  void Compute(OpKernelContext *context) override {
    DCHECK_EQ(context->num_inputs(), 7);

    const Tensor &grad = context->input(0);
    const Tensor &input = context->input(1);
    const Tensor &weights = context->input(2);
    const Tensor &backward_fading_factor = context->input(3);
    const Tensor &group_classes = context->input(4);
    const Tensor &used_neurons = context->input(5);
    const Tensor &groups = context->input(6);

    TensorShape grad_shape = grad.shape();
    TensorShape input_shape = input.shape();
    TensorShape weights_shape = weights.shape();

    Tensor *grad_input = nullptr;
    Tensor *grad_weights = nullptr;

    OP_REQUIRES_OK(context,
                   context->allocate_output(0, input_shape, &grad_input));
    OP_REQUIRES_OK(context,
                   context->allocate_output(1, weights_shape, &grad_weights));

    int features = input_shape.dim_size(1);
    int batch_samples = input_shape.dim_size(0);
    int units = weights_shape.dim_size(1);

    ::tensorflow::functor::SigmaIfGradFunctor<Device, Dtype>::launch(
        context, grad, input, weights, groups, backward_fading_factor,
        group_classes, used_neurons, grad_input, grad_weights, batch_samples,
        units, features);
  }
};

#define REGISTER_CUSTOM_OP(NAME, DEVICE, T)                       \
  REGISTER_KERNEL_BUILDER(                                        \
      Name(#NAME).Device(DEVICE_##DEVICE).TypeConstraint<T>("T"), \
      NAME##Op<DEVICE##Device, T>)

REGISTER_CUSTOM_OP(SigmaIf, CPU, float);
REGISTER_CUSTOM_OP(SigmaIf, CPU, double);
REGISTER_CUSTOM_OP(SigmaIfGrad, CPU, float);
REGISTER_CUSTOM_OP(SigmaIfGrad, CPU, double);

#ifdef GOOGLE_CUDA
REGISTER_CUSTOM_OP(SigmaIf, GPU, float);
REGISTER_CUSTOM_OP(SigmaIf, GPU, double);
REGISTER_CUSTOM_OP(SigmaIfGrad, GPU, float);
REGISTER_CUSTOM_OP(SigmaIfGrad, GPU, double);
#endif  // GOOGLE_CUDA
#undef REGISTER_CUSTOM_OP

} // namespace tensorflow