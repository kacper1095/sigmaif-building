#if GOOGLE_CUDA
#define EIGEN_USE_GPU

#include "cub.cuh"
#include "sigmaif_op.h"
#include "tensorflow/core/util/cuda_kernel_helper.h"

#include <cuda.h>

// https://stackoverflow.com/questions/39274472/error-function-atomicadddouble-double-has-already-been-defined
#if !defined(__CUDA_ARCH__) || __CUDA_ARCH__ >= 600

#else
static __inline__ __device__ double atomicAdd(double *address, double val) {
  unsigned long long int* address_as_ull = (unsigned long long int*)address;
  unsigned long long int old = *address_as_ull, assumed;
  if (val==0.0)
    return __longlong_as_double(old);
  do {
    assumed = old;
    old = atomicCAS(address_as_ull, assumed, __double_as_longlong(val +__longlong_as_double(assumed)));
  } while (assumed != old);
  return __longlong_as_double(old);
}


#endif

#define THREAD_TILE 32
#define MAX_BLOCK_SIZE 512
#define MAX_GRID_SIZE 65535

#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) < (b) ? (a) : (b))
#define CHECK_CUDA_ERROR(ans)                                                  \
  { gpuAssert((ans), __FILE__, __LINE__); }

inline void gpuAssert(cudaError_t code, const char *file, int line,
                      bool abort = true) {
  if (code != cudaSuccess) {
    fprintf(stderr, "Assert: %s %s:%d\n", cudaGetErrorString(code), file, line);
    if (abort) {
      exit(code);
    }
  }
}

namespace tensorflow {
namespace {

template <typename T>
__global__ void
SigmaIfKernel(const T *input, const T *weights, const int *groups,
              const T *threshold, const T *forward_fading_factor,
              const int *group_classes, T *result, int *used_neurons,
              const int batch_samples, const int units, const int features) {
  int ix_sample = threadIdx.x;
  int ix_unit = blockIdx.x;
  if (ix_unit >= units || ix_sample >= batch_samples) {
    return;
  }

  T threshold_ = threshold[0];
  T forward_fading_factor_ = forward_fading_factor[0];

  T accumulated_signal;
  int total_neurons_used;
  T current_fading_factor;

  bool is_aggregated;
  int real_input_index;
  int ix;

  for (; ix_unit < units; ix_unit += gridDim.x) {
    for (; ix_sample < batch_samples; ix_sample += blockDim.x) {
      accumulated_signal = 0.0f;
      total_neurons_used = 0;
      is_aggregated = false;
      real_input_index = 0;

      for (int ix_input = 0; ix_input < features && !is_aggregated;
           ++ix_input) {
        real_input_index = groups[ix_unit * features + ix_input];
        current_fading_factor = ix_input > 0 && group_classes[ix_input] !=
                                                    group_classes[ix_input - 1]
                                    ? forward_fading_factor_
                                    : 1;

        accumulated_signal = accumulated_signal * current_fading_factor +
                             input[ix_sample * features + real_input_index] *
                                 weights[real_input_index * units + ix_unit];

        total_neurons_used++;
        is_aggregated = ix_input < features - 1 &&
                        accumulated_signal >= threshold_ &&
                        group_classes[ix_input] != group_classes[ix_input + 1];
      }

      ix = ix_sample * units + ix_unit;
      result[ix] = accumulated_signal;
      used_neurons[ix] = total_neurons_used;
    }
  }
}

template <typename T>
__global__ void TransposeMatrix(const T *input_data, T *output_data,
                                const int num_rows, const int num_cols) {
  __shared__ T tile[THREAD_TILE][THREAD_TILE + 1];

  int x = blockIdx.x * blockDim.x + threadIdx.x;
  int y = blockIdx.y * blockDim.y + threadIdx.y;

  if (x < num_cols && y < num_rows) {
    int index_in = y * num_cols + x;
    tile[threadIdx.y][threadIdx.x] = input_data[index_in];
    __syncthreads();
  }

  int out_y = blockIdx.x * blockDim.x + threadIdx.y;
  int out_x = blockDim.y * blockIdx.y + threadIdx.x;

  if (out_y < num_cols && out_x < num_rows) {
    int index_out = out_y * num_rows + out_x;
    output_data[index_out] = tile[threadIdx.x][threadIdx.y];
  }
}

__global__ void IndicesRowWise(int *output_data, const int num_rows,
                               const int num_cols) {
  int x = blockIdx.x * blockDim.x + threadIdx.x;
  int y = blockIdx.y * blockDim.y + threadIdx.y;

  if (x >= num_cols || y >= num_rows) {
    return;
  }

  for (int i = y; i < num_rows; i += gridDim.y) {
    for (int j = x; j < num_cols; j += gridDim.x) {
      output_data[i * num_cols + j] = j;
    }
  }
}

// necessary for cub
// because we perform sorting elements of a single row in matrix we need
// a something for a cub, that will index to the next column (for 0 get
// 0th column, for 1 get 1st column etc.) and cub iterates one by one using
// CountingInputIterator
struct SegmentOffsetCreator {
  __host__ __device__ SegmentOffsetCreator(int num_cols)
      : num_cols_(num_cols) {}

  __host__ __device__ __forceinline__ int operator()(int idx) const {
    return idx * num_cols_;
  };

  int num_cols_;
};

template <typename T>
void CubSort(const T *weights, const int *indices, int *sorted_indices,
             const int features, const int units, const cudaStream_t &stream) {
  cub::CountingInputIterator<int> counting_iter(0);
  cub::TransformInputIterator<int, SegmentOffsetCreator,
                              cub::CountingInputIterator<int>>
      segment_offsets_t(counting_iter, SegmentOffsetCreator(features));

  size_t temp_storage_bytes = 0;

  T *d_sorted_weights;
  T *d_weights_to_sort;
  int *d_new_indices;

  CHECK_CUDA_ERROR(
      cudaMalloc((void **)&d_sorted_weights, features * units * sizeof(T)));
  CHECK_CUDA_ERROR(
      cudaMalloc((void **)&d_weights_to_sort, features * units * sizeof(T)));
  CHECK_CUDA_ERROR(
      cudaMalloc((void **)&d_new_indices, units * features * sizeof(int)));

  cudaMemcpy(d_weights_to_sort, weights, features * units * sizeof(T),
             cudaMemcpyDeviceToDevice);

  dim3 transpose_threads_per_block(THREAD_TILE, THREAD_TILE);
  dim3 transpose_num_blocks((units + transpose_threads_per_block.x - 1) /
                                transpose_threads_per_block.x,
                            (features + transpose_threads_per_block.y - 1) /
                                transpose_threads_per_block.y);
  TransposeMatrix<T>
      <<<transpose_num_blocks, transpose_threads_per_block, 0, stream>>>(
          weights, d_weights_to_sort, features, units);

  dim3 iota_threads_per_block(THREAD_TILE, THREAD_TILE);
  dim3 iota_num_blocks(
      min((features + iota_threads_per_block.x - 1) / iota_threads_per_block.x,
          MAX_GRID_SIZE),
      min((units + iota_threads_per_block.y - 1) / iota_threads_per_block.y,
          MAX_GRID_SIZE));
  IndicesRowWise<<<iota_num_blocks, iota_threads_per_block, 0, stream>>>(
      d_new_indices, units, features);

  void *d_temp_storage = nullptr;
  CHECK_CUDA_ERROR(cub::DeviceSegmentedRadixSort::SortPairsDescending(
      /* d_temp_storage */ nullptr,
      /* temp_storage_bytes */ temp_storage_bytes,
      /* d_keys_in */ d_weights_to_sort,
      /* d_keys_out */ d_sorted_weights,
      /* d_values_in */ d_new_indices,
      /* d_values_out */ sorted_indices,
      /* num_items */ features * units,
      /* num_segments */ units,
      /* d_begin_offsets */ segment_offsets_t,
      /* d_end_offsets */ segment_offsets_t + 1,
      /* begin_bit */ 0,
      /* end_bit */ sizeof(T) * 8,
      /* stream */ stream));

  CHECK_CUDA_ERROR(cudaMalloc((void **)&d_temp_storage, temp_storage_bytes));

  CHECK_CUDA_ERROR(cub::DeviceSegmentedRadixSort::SortPairsDescending(
      /* d_temp_storage */ d_temp_storage,
      /* temp_storage_bytes */ temp_storage_bytes,
      /* d_keys_in */ d_weights_to_sort,
      /* d_keys_out */ d_sorted_weights,
      /* d_values_in */ d_new_indices,
      /* d_values_out */ sorted_indices,
      /* num_items */ units * features,
      /* num_segments */ units,
      /* d_begin_offsets */ segment_offsets_t,
      /* d_end_offsets */ segment_offsets_t + 1,
      /* begin_bit */ 0,
      /* end_bit */ sizeof(T) * 8,
      /* stream */ stream));

  CHECK_CUDA_ERROR(cudaFree(d_temp_storage));
  CHECK_CUDA_ERROR(cudaFree(d_weights_to_sort));
  CHECK_CUDA_ERROR(cudaFree(d_sorted_weights));
  CHECK_CUDA_ERROR(cudaFree(d_new_indices));
}

template <typename T>
__global__ void GradKernel(const T *grads, const T *input, const T *weights,
                           const int *used_neurons, const int *groups,
                           const T *backward_fading_factor,
                           const int *group_classes, T *grad_inputs,
                           T *grad_weights, const int batch_samples,
                           const int units, const int features) {
  int ix_unit = blockIdx.x;
  int ix_sample = threadIdx.x;

  T backward_fading_factor_ = backward_fading_factor[0];

  if (ix_unit >= units || ix_sample >= batch_samples)
    return;

  int nb_used_neurons, real_input_index, weights_gradient_index,
      inputs_gradient_index;
  T forward_gradient_value;
  T current_fading_factor;

  for (; ix_unit < units; ix_unit += gridDim.x) {
    for (; ix_sample < batch_samples; ix_sample += blockDim.x) {

      nb_used_neurons = used_neurons[ix_sample * units + ix_unit];
      forward_gradient_value = grads[ix_sample * units + ix_unit];
      current_fading_factor = 1;

      for (int ix_input = nb_used_neurons - 1; ix_input >= 0; --ix_input) {
        real_input_index = groups[ix_unit * features + ix_input];
        weights_gradient_index = real_input_index * units + ix_unit;
        inputs_gradient_index = ix_sample * features + real_input_index;
        atomicAdd(grad_weights + weights_gradient_index, 
            input[ix_sample * features + real_input_index] *
            forward_gradient_value * current_fading_factor);
        atomicAdd(grad_inputs + inputs_gradient_index,
            weights[real_input_index * units + ix_unit] *
            forward_gradient_value * current_fading_factor);
        if (ix_input > 0 &&
            group_classes[ix_input] != group_classes[ix_input - 1]) {
          current_fading_factor *= backward_fading_factor_;
        }
      }
    }
  }
}

} // namespace

namespace functor {
template <typename Dtype> struct SortIndicesFunctor<GPUDevice, Dtype> {
  static void launch(::tensorflow::OpKernelContext *context,
                     const Tensor &weights, const Tensor &groups,
                     const Tensor &resort_groups, Tensor *sorted_indices,
                     int units, int features) {
    auto weights_flat_ptr = weights.flat<Dtype>().data();
    auto groups_flat_ptr = groups.flat<int>().data();
    auto resort_groups_scalar_ptr = resort_groups.flat<bool>().data();
    auto sorted_indices_tensor_ptr = sorted_indices->flat<int>().data();

    const cudaStream_t stream = context->eigen_gpu_device().stream();

    bool *h_resort = (bool *)malloc(sizeof(bool));
    CHECK_CUDA_ERROR(cudaMemcpy(h_resort, resort_groups_scalar_ptr,
                                sizeof(bool), cudaMemcpyDeviceToHost));
    if (h_resort[0]) {
      CubSort<Dtype>(weights_flat_ptr, groups_flat_ptr,
                     sorted_indices_tensor_ptr, features, units, stream);
    } else {
      cudaMemcpy(sorted_indices_tensor_ptr, groups_flat_ptr,
                 features * units * sizeof(int), cudaMemcpyDeviceToDevice);
    }
    free(h_resort);
  }
};

template struct SortIndicesFunctor<GPUDevice, float>;
template struct SortIndicesFunctor<GPUDevice, double>;

template <typename Dtype> struct SigmaIfFunctor<GPUDevice, Dtype> {
  static void launch(::tensorflow::OpKernelContext *context,
                     const Tensor &input, const Tensor &weights,
                     Tensor *sorted_indices, const Tensor &threshold,
                     const Tensor &forward_fading_factor,
                     const Tensor &group_classes, Tensor *output,
                     Tensor *used_neurons, int batch_samples, int units,
                     int features) {
    auto input_flat_ptr = input.flat<Dtype>().data();
    auto weights_flat_ptr = weights.flat<Dtype>().data();
    auto threshold_scalar_ptr = threshold.flat<Dtype>().data();
    auto forward_fading_factor_scalar_ptr =
        forward_fading_factor.flat<Dtype>().data();
    auto group_classes_vector_ptr = group_classes.flat<int>().data();

    auto output_flat_ptr = output->flat<Dtype>().data();
    auto used_neurons_flat_ptr = used_neurons->flat<int>().data();
    auto sorted_indices_tensor_ptr = sorted_indices->flat<int>().data();

    SigmaIfKernel<Dtype>
        <<<min(units, MAX_GRID_SIZE), min(batch_samples, MAX_BLOCK_SIZE), 0,
           context->eigen_gpu_device().stream()>>>(
            input_flat_ptr, weights_flat_ptr, sorted_indices_tensor_ptr,
            threshold_scalar_ptr, forward_fading_factor_scalar_ptr,
            group_classes_vector_ptr, output_flat_ptr, used_neurons_flat_ptr,
            batch_samples, units, features);
  }
};

template struct SigmaIfFunctor<GPUDevice, float>;
template struct SigmaIfFunctor<GPUDevice, double>;

template <typename Dtype> struct SigmaIfGradFunctor<GPUDevice, Dtype> {
  static void launch(::tensorflow::OpKernelContext *context, const Tensor &grad,
                     const Tensor &input, const Tensor &weights,
                     const Tensor &groups, const Tensor &backward_fading_factor,
                     const Tensor &group_classes, const Tensor &used_neurons,
                     Tensor *grad_input, Tensor *grad_weights,
                     int batch_samples, int units, int features) {

    auto grad_tensor_ptr = grad.flat<Dtype>().data();
    auto input_tensor_ptr = input.flat<Dtype>().data();
    auto weights_tensor_ptr = weights.flat<Dtype>().data();
    auto used_neurons_tensor_ptr = used_neurons.flat<int>().data();
    auto groups_tensor_ptr = groups.flat<int>().data();
    auto backward_fading_factor_scalar_ptr =
        backward_fading_factor.flat<Dtype>().data();
    auto group_classes_vector_ptr = group_classes.flat<int>().data();

    auto grad_input_tensor_ptr = grad_input->flat<Dtype>().data();
    auto grad_weights_tensor_ptr = grad_weights->flat<Dtype>().data();

    cudaMemset(grad_input_tensor_ptr, 0,
               batch_samples * features * sizeof(Dtype));
    cudaMemset(grad_weights_tensor_ptr, 0, features * units * sizeof(Dtype));
    GradKernel<Dtype>
        <<<min(units, MAX_GRID_SIZE), min(batch_samples, MAX_BLOCK_SIZE), 0,
           context->eigen_gpu_device().stream()>>>(
            grad_tensor_ptr, input_tensor_ptr, weights_tensor_ptr,
            used_neurons_tensor_ptr, groups_tensor_ptr,
            backward_fading_factor_scalar_ptr, group_classes_vector_ptr,
            grad_input_tensor_ptr, grad_weights_tensor_ptr, batch_samples,
            units, features);
  }
};

template struct SigmaIfGradFunctor<GPUDevice, float>;
template struct SigmaIfGradFunctor<GPUDevice, double>;

} // namespace functor

} // namespace tensorflow

#endif // GOOGLE_CUDA