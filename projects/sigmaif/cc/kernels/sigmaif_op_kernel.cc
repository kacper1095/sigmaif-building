#include "tensorflow/core/util/work_sharder.h"

#include "sigmaif_op.h"
#include <algorithm>
#include <cstring>

namespace tensorflow {
namespace functor {

template <typename Dtype> struct SortIndicesFunctor<CPUDevice, Dtype> {
  static void launch(::tensorflow::OpKernelContext *context,
                     const Tensor &weights, const Tensor &groups,
                     const Tensor &resort_groups, Tensor *sorted_indices,
                     int units, int features) {
    auto weights_tensor = weights.matrix<Dtype>();
    auto groups_tensor = groups.matrix<int>();
    auto resort_groups_scalar = resort_groups.scalar<bool>();

    auto sorted_indices_tensor = sorted_indices->matrix<int>();

    if (resort_groups_scalar(0)) {
      auto SortIndices = [&, features]() {
        for (int unit_i = 0; unit_i < units; ++unit_i) {
          const auto comp = [weights_tensor, unit_i](const int a, const int b) {
            return weights_tensor(a, unit_i) > weights_tensor(b, unit_i);
          };

          auto *begin = &sorted_indices_tensor(unit_i, 0);
          auto *end = &sorted_indices_tensor(unit_i, features);

          std::iota(begin, end, 0);
          std::sort(begin, end, comp);
        }
      };
      SortIndices();
    } else {
      std::memcpy(sorted_indices_tensor.data(), groups_tensor.data(),
                  sizeof(int) * units * features);
    }
  }
};

template struct SortIndicesFunctor<CPUDevice, float>;
template struct SortIndicesFunctor<CPUDevice, double>;

template <typename Dtype> struct SigmaIfFunctor<CPUDevice, Dtype> {
  static void launch(::tensorflow::OpKernelContext *context,
                     const Tensor &input, const Tensor &weights,
                     Tensor *sorted_indices, const Tensor &threshold,
                     const Tensor &forward_fading_factor,
                     const Tensor &group_classes, Tensor *output,
                     Tensor *used_neurons, int batch_samples, int units,
                     int features) {
    auto input_tensor = input.matrix<Dtype>();
    auto weights_tensor = weights.matrix<Dtype>();
    auto threshold_scalar = threshold.scalar<Dtype>();
    auto forward_fading_factor_scalar = forward_fading_factor.scalar<Dtype>();
    auto group_classes_vector = group_classes.vec<int>();

    auto worker_threads = *(context->device()->tensorflow_cpu_worker_threads());
    const int64 total_units = batch_samples * units;

    // 2 because we need to read <features> times and write <features> times
    const int64 cost_per_unit = features * 3;
    auto out_tensor_ptr = output->flat<Dtype>().data();
    auto used_neurons_tensor_ptr = used_neurons->flat<int>().data();
    auto sorted_indices_tensor_ptr = sorted_indices->flat<int>().data();

    Shard(worker_threads.num_threads, worker_threads.workers, total_units,
          cost_per_unit,
          [&, batch_samples, units, features](int start, int limit) {
            int real_input_index;
            int sample_index;
            int unit_index;
            bool is_aggregated;
            Dtype current_fading_factor;
            for (int i = start; i < limit; ++i) {
              sample_index = i / units;
              unit_index = i % units;
              if (sample_index >= batch_samples || unit_index >= units) {
                continue;
              }
              is_aggregated = false;

              out_tensor_ptr[i] = 0;
              used_neurons_tensor_ptr[i] = 0;
              for (int input_index = 0;
                   input_index < features && !is_aggregated; ++input_index) {
                real_input_index =
                    sorted_indices_tensor_ptr[unit_index * features +
                                              input_index];

                current_fading_factor =
                    input_index > 0 && group_classes_vector(input_index) !=
                                           group_classes_vector(input_index - 1)
                        ? forward_fading_factor_scalar(0)
                        : 1;
                out_tensor_ptr[i] =
                    out_tensor_ptr[i] * current_fading_factor +
                    input_tensor(sample_index, real_input_index) *
                        weights_tensor(real_input_index, unit_index);
                used_neurons_tensor_ptr[i]++;

                is_aggregated = out_tensor_ptr[i] >= threshold_scalar(0) &&
                                input_index < features - 1 &&
                                group_classes_vector(input_index) !=
                                    group_classes_vector(input_index + 1);
              }
            }
          }); // Shard
  }
};

template struct SigmaIfFunctor<CPUDevice, float>;
template struct SigmaIfFunctor<CPUDevice, double>;

template <typename Dtype> struct SigmaIfGradFunctor<CPUDevice, Dtype> {
  static void launch(::tensorflow::OpKernelContext *context, const Tensor &grad,
                     const Tensor &input, const Tensor &weights,
                     const Tensor &groups, const Tensor &backward_fading_factor,
                     const Tensor &group_classes, const Tensor &used_neurons,
                     Tensor *grad_input, Tensor *grad_weights,
                     int batch_samples, int units, int features) {

    auto grad_input_ = grad_input->flat<Dtype>();
    auto grad_weights_ = grad_weights->flat<Dtype>();

    grad_input_.setZero();
    grad_weights_.setZero();

    auto grad_tensor = grad.matrix<Dtype>();
    auto input_tensor = input.matrix<Dtype>();
    auto weights_tensor = weights.matrix<Dtype>();
    auto groups_tensor = groups.matrix<int>();
    auto backward_fading_factor_scalar = backward_fading_factor.scalar<Dtype>();
    auto group_classes_vector = group_classes.vec<int>();

    auto grad_input_ptr = grad_input_.data();
    auto grad_weights_ptr = grad_weights_.data();
    auto used_neurons_ptr = used_neurons.flat<int>().data();

    auto worker_threads = *(context->device()->tensorflow_cpu_worker_threads());
    const int64 total_units = batch_samples * units;
    const int64 cost_per_unit = features * 4;

    Shard(
        worker_threads.num_threads, worker_threads.workers, total_units,
        cost_per_unit,
        [&, batch_samples, units, features](int start, int limit) {
          int real_input_index;
          int nb_used_neurons;
          int sample_index;
          int unit_index;
          Dtype *grad_input_offset;
          Dtype *grad_weights_offset;
          Dtype current_fading_factor;

          for (int i = start; i < limit; ++i) {
            sample_index = i / units;
            unit_index = i % units;
            nb_used_neurons =
                *(used_neurons_ptr + sample_index * units + unit_index);
            current_fading_factor = 1;
            for (int input_group_i = nb_used_neurons - 1; input_group_i >= 0;
                 --input_group_i) {
              real_input_index = groups_tensor(unit_index, input_group_i);

              grad_input_offset =
                  grad_input_ptr + sample_index * features + real_input_index;
              grad_weights_offset =
                  grad_weights_ptr + real_input_index * units + unit_index;

              *grad_input_offset +=
                  weights_tensor(real_input_index, unit_index) *
                  grad_tensor(sample_index, unit_index) * current_fading_factor;
              *grad_weights_offset +=
                  input_tensor(sample_index, real_input_index) *
                  grad_tensor(sample_index, unit_index) * current_fading_factor;

              if (input_group_i > 0 &&
                  group_classes_vector(input_group_i) !=
                      group_classes_vector(input_group_i - 1)) {
                current_fading_factor *= backward_fading_factor_scalar(0);
              }
            }
          }
        }

    );
  }
};

template struct SigmaIfGradFunctor<CPUDevice, float>;
template struct SigmaIfGradFunctor<CPUDevice, double>;

} // namespace functor
} // namespace tensorflow