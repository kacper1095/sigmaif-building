#ifndef SIGMAIF_OP_KERNELS_SIGMAIF_OP_H_
#define SIGMAIF_OP_KERNELS_SIGMAIF_OP_H_

#include "tensorflow/core/framework/op_kernel.h"

namespace tensorflow {

typedef Eigen::ThreadPoolDevice CPUDevice;
typedef Eigen::GpuDevice GPUDevice;

namespace functor {

template <typename Device, typename Dtype> struct SortIndicesFunctor {
  static void launch(::tensorflow::OpKernelContext *context,
                     const Tensor &weights, 
                     const Tensor &groups,
                     const Tensor &resort_groups, 
                     Tensor *sorted_indices,
                     int units, 
                     int features);
};

template <typename Device, typename Dtype> struct SigmaIfFunctor {
  static void launch(::tensorflow::OpKernelContext *context,
                     const Tensor &input, const Tensor &weights,
                     Tensor *sorted_indices, const Tensor &threshold,
                     const Tensor &forward_fading_factor,
                     const Tensor &group_classes, Tensor *output,
                     Tensor *used_neurons, int batch_samples, int units,
                     int features);
};

template <typename Device, typename Dtype> struct SigmaIfGradFunctor {
  static void launch(::tensorflow::OpKernelContext *context, const Tensor &grad,
                     const Tensor &input, const Tensor &weights,
                     const Tensor &groups, const Tensor &backward_fading_factor,
                     const Tensor &group_classes, const Tensor &used_neurons,
                     Tensor *grad_input, Tensor *grad_weights,
                     int batch_samples, int units, int features);
};
} // namespace functor
} // namespace tensorflow
#endif