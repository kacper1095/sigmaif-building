#include <stdio.h>
#include <iostream>


template <typename T>
void PrintMatrix(const T* data, int num_rows, int num_cols){
  for(int y = 0; y < num_rows; y++) {
    for(int x = 0; x < num_cols; x++) {
      std::cout << data[y * num_cols + x] << " ";
    }
    std::cout << std::endl;
  }
}

template <typename T>
void CudaPrintMatrix(const T* d_data, int num_rows, int num_cols) {
  T* h_data = (T*)malloc(sizeof(T) * num_rows * num_cols);
  cudaMemcpy(h_data, d_data, sizeof(T) * num_cols * num_rows, cudaMemcpyDeviceToHost);
  PrintMatrix<T>(h_data, num_rows, num_cols);
  free(h_data);
}