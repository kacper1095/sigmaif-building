FROM tensorflow/tensorflow:1.9.0-gpu-py3 

RUN apt-get update \
    && apt-get install -y \
        vim \
        wget \
        git \
        gcc-5 g++-5 \ 
        && update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-5 100 \ 
        && update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-5 100

# switching gcc is necessary since it's the original gcc that's tensorflow is compiled against

ENV CMAKE_VERSION 3.12
ENV CMAKE_BUILD 3

RUN mkdir /temp \
    && cd /temp \
    && wget https://cmake.org/files/v${CMAKE_VERSION}/cmake-${CMAKE_VERSION}.${CMAKE_BUILD}.tar.gz \
    && tar -xzvf cmake-${CMAKE_VERSION}.${CMAKE_BUILD}.tar.gz \
    && cd cmake-${CMAKE_VERSION}.${CMAKE_BUILD} \
    && ./bootstrap \
    && make -j8 \
    && make install \
    && cp bin/cmake /usr/bin/cmake

RUN cd /usr/local/cuda/lib64 \
    && mv stubs/libcuda.so ./ \
    && ln -s libcuda.so libcuda.so.1 \
    && ldconfig

WORKDIR /root/

RUN wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
    && chmod +x Miniconda3-latest-Linux-x86_64.sh \
    && ./Miniconda3-latest-Linux-x86_64.sh -b 

ENV PYTHONPATH=/root/miniconda3/bin
ENV PATH=/root/miniconda3/bin:$PATH

RUN conda install -y \
    python=3.6 \
    Pillow \
    h5py \
    ipykernel \
    jupyter \
    matplotlib \
    numpy \
    pandas \
    scipy \
    scikit-learn \
    && python -m ipykernel.kernelspec

RUN conda install -c anaconda -y tensorflow-gpu=1.9.0

WORKDIR /projects
COPY . .
RUN mkdir build

CMD ["bash"]

