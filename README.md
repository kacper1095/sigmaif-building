# Tensorflow docker building environment
## Description

This repo was created to run precompiled tensorflow-gpu v1.9.0 to use it to compile custom layers / ops etc. I'm not sure whether this will work with versions > 1.9.0.


## Requirements

- docker 18.09 (every version above it should be fine) - [link](https://docs.docker.com/install/) to installation 
instruction
- docker-compose 1.22.0 (every version above it should be fine) - [link](https://github.com/Yelp/docker-compose/blob/master/docs/install.md) to installation instruction
- nvidia driver 390.77

## Running

### SigmaIf compilation 
1. In main folder run following commands
```
docker-compose build #builds necessary images
docker-compose run app #enters docker to perform compilation
```
2. Go to folder `projects/sigmaif/build/`. Create `build` folder if it doesn't exist.
3. `cmake ..`
4. `make`
5. If everything is ok, in your current folder `projects/sigmaif/build` library `libsigmaif_op.so` should be created.
6. Exit docker using `ctrl+c` or `ctrl+d`.
7. Copy `projects/sigmaif/build/libsigma_if.so` to [sigmaif-tensorflow](https://gitlab.com/kacper1095/sigmaif-tensorflow) cloned repository to folder `sigmaif/libs`.
8. Now you can proceed to python package installation. 

### Any op / layer  compilation
1. Copy your files to `projects` to compile it later. (You can use https://github.com/taxfromdk/tensorflow_dense_op as a base, you need delete first two `set`s in `CMakeLists.txt`)
2. `docker-compose build`
3. `docker-compose run app`
4. Move to your projects and build it.
5. `projects` directory is linked to local directory, so compiled files will be there once you exit container.
